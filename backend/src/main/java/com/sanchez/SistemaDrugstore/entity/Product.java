package com.sanchez.SistemaDrugstore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product {
@Id
@GeneratedValue(strategy= GenerationType.IDENTITY )
private Integer productId;
@Column(name="name")
private String name;
@Column(name="price", columnDefinition="Decimal(8,2)") 
private double price;
@Column(name="stock")
private Integer stock;
@Column(name="deleted")
private boolean deleted;

public Product() {
	
}


public Product(Integer productId, String name, double price, Integer stock, boolean deleted) {
	this.productId = productId;
	this.name = name;
	this.price = price;
	this.stock = stock;
	this.deleted = deleted;
}


public Integer getProductId() {
	return productId;
}


public void setProductId(Integer productId) {
	this.productId = productId;
}


public String getName() {
	return name;
}


public void setName(String name) {
	this.name = name;
}


public double getPrice() {
	return price;
}


public void setPrice(double price) {
	this.price = price;
}


public Integer getStock() {
	return stock;
}


public void setStock(Integer stock) {
	this.stock = stock;
}


public boolean isDeleted() {
	return deleted;
}


public void setDeleted(boolean deleted) {
	this.deleted = deleted;
}


public boolean altaProducto() {
	
	if(deleted) {
		return true;
	}else
	return false;
	
}


}
