package com.sanchez.SistemaDrugstore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer")

public class Customer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer customerId;
	@Column(name="firsName")
	private String firstName;
	@Column(name="lastName")
	private String lastName;
	@Column(name="addres")
	private String addres;
	@Column(name="email")
	private String email;
	@Column(name="deleted")
	private boolean deleted;
	
	
	
	
	public Customer() {
		
	}
	
	public Customer(Integer customerId, String firstName, String lastName, String addres, String email,
			boolean deleted) {
		super();
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.addres = addres;
		this.email = email;
		this.deleted = deleted;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddres() {
		return addres;
	}
	public void setAddres(String addres) {
		this.addres = addres;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	public boolean altaCustomer() {
		
		if(deleted) {
			return true;
		}
		
		return false;
		
	}
	
	

}
