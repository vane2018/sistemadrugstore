package com.sanchez.SistemaDrugstore.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="invoice")
public class Invoice {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer invoiceId;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate dateTime;
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	
	@OneToMany(mappedBy = "invoice", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
	CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true)
	//@OneToMany(mappedBy="invoice",fetch=FetchType.LAZY,cascade=CascadeType.ALL,orphanRemoval=true)
	private List<InvoiceDetail> invoiceDetail;

	
	

	public Invoice() {
	
		
	}




	public Invoice(Integer invoiceId, LocalDate dateTime, Customer customer) {
		super();
		this.invoiceId = invoiceId;
		this.dateTime = dateTime;
		this.customer = customer;
	}




	public Integer getInvoiceId() {
		return invoiceId;
	}




	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}




	public LocalDate getDateTime() {
		return dateTime;
	}




	public void setDateTime(LocalDate dateTime) {
		this.dateTime = dateTime;
	}




	public Customer getCustomer() {
		return customer;
	}




	public void setCustomer(Customer customer) {
		this.customer = customer;
	}




	public List<InvoiceDetail> getInvoiceDetail() {
		return invoiceDetail;
	}




	public void setInvoiceDetail(List<InvoiceDetail> invoiceDetail) {
		this.invoiceDetail = invoiceDetail;
	}




	

}
