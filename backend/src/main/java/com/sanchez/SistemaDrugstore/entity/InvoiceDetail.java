package com.sanchez.SistemaDrugstore.entity;

import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sanchez.SistemaDrugstore.service.ProductService;

@Entity
@Table(name="invoice_detail")
public class InvoiceDetail {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer invoiceDetalleId;
	
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name="invoice_Id")
	private Invoice invoice;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private Product product;
	
	private Integer quantity;
	
	



	public InvoiceDetail(Integer invoiceDetalleId, Invoice invoice, Product product, Integer quantity) {
		super();
		this.invoiceDetalleId = invoiceDetalleId;
		this.invoice = invoice;
		this.product = product;
		this.quantity = quantity;
	}




	public InvoiceDetail(Product product, Integer quantity) {
		super();
		this.product = product;
		this.quantity = quantity;
	}




	public InvoiceDetail() {
		
	}



	public Integer getInvoiceDetalleId() {
		return invoiceDetalleId;
	}



	public void setInvoiceDetalleId(Integer invoiceDetalleId) {
		this.invoiceDetalleId = invoiceDetalleId;
	}



	public Invoice getInvoice() {
		return invoice;
	}



	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}



	public Product getProduct() {
		return product;
	}



	public void setProduct(Product product) {
		this.product = product;
	}



	public Integer getQuantity() {
		return quantity;
	}



	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	public double subtotal() {
		return getQuantity()* product.getPrice();
	}

	public boolean validarCantidad() {
		if(getQuantity()>0) {
			return true;
			
		    }return false;
	}
	
	public boolean validarQuantityStock(Integer stock, Integer cantidad) {

		if(stock==null && cantidad>0) {
			return true;
		}
		
		
		return false;
	}
	
	
	public boolean validateProduct(Integer idProduct) {
		


		
		if(idProduct>0)
		 {
			return true;
		}
		return false;
	}
	
	
	
	

}
