package com.sanchez.SistemaDrugstore.dto;

public class ProductDTO {
	Double price;
	Integer stock;
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	
	
	
	
}
