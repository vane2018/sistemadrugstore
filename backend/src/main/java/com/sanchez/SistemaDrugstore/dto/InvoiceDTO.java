package com.sanchez.SistemaDrugstore.dto;

import java.time.LocalDate;
import java.util.List;

import com.sanchez.SistemaDrugstore.entity.Customer;
import com.sanchez.SistemaDrugstore.entity.Invoice;
import com.sanchez.SistemaDrugstore.entity.InvoiceDetail;
import com.sanchez.SistemaDrugstore.entity.Product;

public class InvoiceDTO {
	private LocalDate dateTime;
	public LocalDate getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDate dateTime) {
		this.dateTime = dateTime;
	}

	Integer idCustomer;
	List<Integer> idProd;
    Integer cantidad;
 




	
	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


	

	

	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	public List<Integer> getIdProd() {
		return idProd;
	}

	public void setIdProd(List<Integer> idProd) {
		this.idProd = idProd;
	}



	
	
	
	
}
