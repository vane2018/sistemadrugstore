package com.sanchez.SistemaDrugstore.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sanchez.SistemaDrugstore.entity.Product;
import com.sanchez.SistemaDrugstore.service.ProductService;
import com.sanchez.SistemaDrugstore.web.rest.util.HeaderUtil;
import com.sanchez.SistemaDrugstore.web.rest.util.ResponseUtil;

@RestController
@RequestMapping("/tp")
public class ProductResource {
	
	private final Logger log = LoggerFactory.getLogger(ProductResource.class);

	  private static final String ENTITY_NAME = "product";

	  private final ProductService productService;

	  public ProductResource(ProductService productService) {
	    this.productService = productService;
	  }

	  /**
	   *
	   * @return
	   */
	  @GetMapping("/products")
	  public ResponseEntity<List<Product>> getAllProductos() {
	    log.debug("REST request to get a page of Productos");
	    List<Product> page = productService.findAll();
	    return new ResponseEntity<>(page, HttpStatus.OK);
	  }

	  @PostMapping("/products")
	  public ResponseEntity<Product> createProduct(@RequestBody Product product) throws URISyntaxException {
	    log.debug("REST request to save Producto : {}", product);

	    Product result = productService.save(product);
	    return ResponseEntity.created(new URI("/tp/products/" + result.getProductId()))
	        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getProductId().toString()))
	        .body(result);
	  }

	  @PutMapping("/products")
	  public ResponseEntity<Product> updateProduct(@RequestBody Product product) throws URISyntaxException {
	    log.debug("REST request to update Producto : {}", product);

	    Product result = productService.save(product);
	    return ResponseEntity.ok()
	        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, product.getProductId().toString()))
	        .body(result);
	  }

	  @GetMapping("/products/{productId}")
	  public ResponseEntity<Product> getProduct(@PathVariable Integer productId) {
	    log.debug("REST request to get Producto : {}", productId);
	    Optional<Product> productDTO = productService.findOne(productId);
	    return ResponseUtil.wrapOrNotFound(productDTO);
	  }

	  @DeleteMapping("/products/{productId}")
	  public ResponseEntity<Void> deleteProducto(@PathVariable Integer productId) {
	    log.debug("REST request to delete Producto : {}", productId);
	    productService.delete(productId);
	    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, productId.toString())).build();
	  }
	  
	  @GetMapping("/products/search")
	  public ResponseEntity<List<Product>> getProduct() {
	    log.debug("REST request to get a page of Productos");
	    List<Product> page = productService.productSearch();
	    return new ResponseEntity<>(page, HttpStatus.OK);
	  }
	  
	  @GetMapping("/products/getTopSelling")
	  public ResponseEntity<List<Product>> getMaxQuantity() {
	    log.debug("REST request to get a page of Productos");
	    List<Product> page = productService.GetSelling();
	    return new ResponseEntity<>(page, HttpStatus.OK);
	  }
	

}
