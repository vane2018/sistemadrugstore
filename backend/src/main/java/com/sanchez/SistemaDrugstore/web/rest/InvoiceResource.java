package com.sanchez.SistemaDrugstore.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sanchez.SistemaDrugstore.dto.InvoiceDTO;
import com.sanchez.SistemaDrugstore.entity.Invoice;

import com.sanchez.SistemaDrugstore.service.InvoiceService;
import com.sanchez.SistemaDrugstore.web.rest.util.HeaderUtil;
import com.sanchez.SistemaDrugstore.web.rest.util.ResponseUtil;


@RestController
@RequestMapping("/tp")
public class InvoiceResource {
	
	private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

	  private static final String ENTITY_NAME = "customer";

	  private final InvoiceService invoiceService;



	
	public InvoiceResource(InvoiceService invoiceService) {
		super();
		this.invoiceService = invoiceService;
	}

	/**
	   *
	   * @return
	   */
	  @GetMapping("/invoices")
	  public ResponseEntity<List<Invoice>> getAllInvoice() {
	    log.debug("REST request to get a page of Invoice");
	    List<Invoice> page = invoiceService.findAll();
	    return new ResponseEntity<>(page, HttpStatus.OK);
	  }
@PostMapping(value="/invoices", consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	 // @PostMapping("/invoices")
	  public ResponseEntity<Object> createInvoice(@RequestBody Invoice invoice) throws URISyntaxException {
	    log.debug("REST request to save Invoice :{}", invoice);

	    Invoice result = invoiceService.save(invoice);
	    return ResponseEntity.created(new URI("/tp/invoices/" + result.getInvoiceId()))
	        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getInvoiceId().toString()))
	        .body(result);
	  }

	  @PutMapping("/invoices")
	  public ResponseEntity<Invoice> updateCustomer(@RequestBody Invoice invoice) throws URISyntaxException {
	    log.debug("REST request to update Customer : {}", invoice);

	    Invoice result = invoiceService.save(invoice);
	    return ResponseEntity.ok()
	        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invoice.getInvoiceId().toString()))
	        .body(result);
	  }

	  @GetMapping("/invoices/{invoiceId}")
	  public ResponseEntity<Invoice> getInvoice(@PathVariable Integer invoiceId) {
	    log.debug("REST request to get Customer : {}", invoiceId);
	    Optional<Invoice> invoiceDTO = invoiceService.findOne(invoiceId);
	    return ResponseUtil.wrapOrNotFound(invoiceDTO);
	  }

	  @DeleteMapping("/invoices/{invoiceId}")
	  public ResponseEntity<Void> deleteInvoice(@PathVariable Integer invoiceId) {
	    log.debug("REST request to delete Producto : {}", invoiceId);
	    invoiceService.delete(invoiceId);
	    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, invoiceId.toString())).build();
	  }
	


}
