package com.sanchez.SistemaDrugstore.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sanchez.SistemaDrugstore.entity.Product;
import com.sanchez.SistemaDrugstore.entity.User;
import com.sanchez.SistemaDrugstore.service.UserService;
import com.sanchez.SistemaDrugstore.web.rest.util.HeaderUtil;
import com.sanchez.SistemaDrugstore.web.rest.util.ResponseUtil;


@RestController
@RequestMapping("/tp")
public class UserResource {
	private final Logger log = LoggerFactory.getLogger(UserResource.class);

	  private static final String ENTITY_NAME = "user";

	  private final UserService userService;

	 

	  public UserResource(UserService userService) {
		this.userService = userService;
	}

	/**
	   *
	   * @return
	   */
	  @GetMapping("/users")
	  public ResponseEntity<List<User>> getAllUsers() {
	    log.debug("REST request to get a page of Productos");
	    List<User> page = userService.findAll();
	    return new ResponseEntity<>(page, HttpStatus.OK);
	  }

	  @PostMapping("/users")
	  public ResponseEntity<User> createUser(@RequestBody User user) throws URISyntaxException {
	    log.debug("REST request to save Producto : {}", user);

	    User result = userService.save(user);
	    return ResponseEntity.created(new URI("/tp/users/" + result.getUserId()))
	        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getUserName().toString()))
	        .body(result);
	  }

	  @PutMapping("/users")
	  public ResponseEntity<User> updateUser(@RequestBody User user) throws URISyntaxException {
	    log.debug("REST request to update Producto : {}", user);

	    User result = userService.save(user);
	    return ResponseEntity.ok()
	        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, user.getUserId().toString()))
	        .body(result);
	  }

	  @GetMapping("/users/{userId}")
	  public ResponseEntity<User> getUser(@PathVariable Integer userId) {
	    log.debug("REST request to get Producto : {}", userId);
	    Optional<User> userDTO = userService.findOne(userId);
	    return ResponseUtil.wrapOrNotFound(userDTO);
	  }
	  
	  @GetMapping("/users/search/{name}/{pass}")
	  public ResponseEntity<User> buscaUsuario(@PathVariable  String name,@PathVariable  String pass) {
	    log.debug("REST request to get Producto : {}", name,pass);
	    User userDTO = userService.searchUser(name, pass);
	    return new ResponseEntity<>(userDTO, HttpStatus.OK);
	  }

	  @DeleteMapping("/users/{userId}")
	  public ResponseEntity<Void> deleteUser(@PathVariable Integer userId) {
	    log.debug("REST request to delete Producto : {}", userId);
	    userService.delete(userId);
	    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, userId.toString())).build();
	  }
	

	
}
