package com.sanchez.SistemaDrugstore.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sanchez.SistemaDrugstore.entity.Customer;
import com.sanchez.SistemaDrugstore.service.CustomerService;

import com.sanchez.SistemaDrugstore.web.rest.util.HeaderUtil;
import com.sanchez.SistemaDrugstore.web.rest.util.ResponseUtil;


@RestController
@RequestMapping("/tp")
public class CustomerResource {
	private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

	  private static final String ENTITY_NAME = "customer";

	  private final CustomerService customerService;



	  public CustomerResource(CustomerService customerService) {
	
		this.customerService = customerService;
	}

	/**
	   *
	   * @return
	   */
	  @GetMapping("/customers")
	  public ResponseEntity<List<Customer>> getAllCustomer() {
	    log.debug("REST request to get a page of Productos");
	    List<Customer> page = customerService.findAll();
	    return new ResponseEntity<>(page, HttpStatus.OK);
	  }

	  @PostMapping("/customers")
	  public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) throws URISyntaxException {
	    log.debug("REST request to save Customer : {}", customer);

	    Customer result = customerService.save(customer);
	    return ResponseEntity.created(new URI("/tp/customers/" + result.getCustomerId()))
	        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getCustomerId().toString()))
	        .body(result);
	  }

	  @PutMapping("/customers")
	  public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer) throws URISyntaxException {
	    log.debug("REST request to update Customer : {}", customer);

	    Customer result = customerService.save(customer);
	    return ResponseEntity.ok()
	        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customer.getCustomerId().toString()))
	        .body(result);
	  }

	  @GetMapping("/customers/{customerId}")
	  public ResponseEntity<Customer> getCustomer(@PathVariable Integer customerId) {
	    log.debug("REST request to get Customer : {}", customerId);
	    Optional<Customer> customerDTO = customerService.findOne(customerId);
	    return ResponseUtil.wrapOrNotFound(customerDTO);
	  }

	  @DeleteMapping("/customers/{customerId}")
	  public ResponseEntity<Void> deleteCustomer(@PathVariable Integer customerId) {
	    log.debug("REST request to delete Producto : {}", customerId);
	    customerService.delete(customerId);
	    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, customerId.toString())).build();
	  }
	

}
