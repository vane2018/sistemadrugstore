package com.sanchez.SistemaDrugstore.repository;

import java.util.Optional;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sanchez.SistemaDrugstore.entity.User;


public interface UserRepository extends JpaRepository<User,Integer>{
	@Query(nativeQuery=true, value="select * from User where user_name = :usuario and pass= :password")
	User verificarNombreUsuario(@Param("usuario") String usuario,@Param("password") String password);

	

}
