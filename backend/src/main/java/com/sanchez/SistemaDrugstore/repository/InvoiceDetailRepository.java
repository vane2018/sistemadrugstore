package com.sanchez.SistemaDrugstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sanchez.SistemaDrugstore.entity.InvoiceDetail;



public interface InvoiceDetailRepository extends JpaRepository<InvoiceDetail,Integer> {
	//@Query(value="Insert INTO invoice_detail(quantity,invoice_id,product_id) VALUES(:quantity,:invoiceId,:productId)",nativeQuery=true)
	//int registrar(@Param("quantity") Integer quantity,@Param("invoiceId") Integer invoiceId,@Param("productId") Integer productId );


}
