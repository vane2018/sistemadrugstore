package com.sanchez.SistemaDrugstore.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sanchez.SistemaDrugstore.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Integer>{
Optional<Customer> findById(Integer idCustomer);
}
