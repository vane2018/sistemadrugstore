package com.sanchez.SistemaDrugstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sanchez.SistemaDrugstore.entity.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice,Integer>{

	
	
}
