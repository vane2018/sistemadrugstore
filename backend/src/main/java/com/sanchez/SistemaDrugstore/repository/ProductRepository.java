package com.sanchez.SistemaDrugstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sanchez.SistemaDrugstore.entity.Product;

public interface ProductRepository extends JpaRepository <Product,Integer> {
         
  @Query(nativeQuery=true, value="select * from Product where stock>0 and deleted=false")
	    public List<Product> searchProduct();
	
	
  @Query(nativeQuery=true, value=" SELECT p.product_id,p.deleted,p.name,p.price,p.stock FROM product as p where p.product_id = (select d.product_id from invoice_detail as d where d.quantity=(select max(quantity) from invoice_detail))")
	      public List<Product> Selling();
  
  @Query(nativeQuery=true, value="select p.stock from Product as p where product_id = idProduct")
          public Integer controlStock(@Param("idProduct") Integer idproducto); 
 
  
}
