package com.sanchez.SistemaDrugstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaDrugstoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaDrugstoreApplication.class, args);
	}
}
