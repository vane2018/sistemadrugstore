package com.sanchez.SistemaDrugstore.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sanchez.SistemaDrugstore.entity.Product;
import com.sanchez.SistemaDrugstore.repository.ProductRepository;
import com.sanchez.SistemaDrugstore.service.ProductService;


@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	
	private final ProductRepository productRepository;
	
	
	//private final Product product;
	
	



	public ProductServiceImpl(ProductRepository productRepository) {
		super();
		this.productRepository = productRepository;
	}

	@Override
	public Product save(Product product) {
		// TODO Auto-generated method stub
		if(validate(product) ) {
			return this.productRepository.save(product);
		}
		return null;
	}

	@Override
	public List<Product> findAll() {
		// TODO Auto-generated method stub
		return this.productRepository.findAll();
	}

	@Override
	public Optional<Product> findOne(Integer productId) {
		// TODO Auto-generated method stub
		return this.productRepository.findById(productId);
	}

	@Override
	public void delete(Integer productId) {
		   
		   Optional<Product>produ= productRepository.findById(productId);
	      	Product p= produ.get();
	      	p.setDeleted(false);
		
	}

	@Override
	public List<Product> productSearch() {
		// TODO Auto-generated method stub
		return this.productRepository.searchProduct();
	}

	@Override
	public List<Product> GetSelling() {
		// TODO Auto-generated method stub
		return this.productRepository.Selling();
	}

	//@Override
	public boolean validate(Product product) {
		// TODO Auto-generated method stub
		if ((product.getStock()>0)&(product.getPrice()>0)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean stockValidar(Integer stock) {
if (stock>0) {
	return true;
}else return false;


}
}
