package com.sanchez.SistemaDrugstore.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sanchez.SistemaDrugstore.entity.Customer;
import com.sanchez.SistemaDrugstore.entity.Product;
import com.sanchez.SistemaDrugstore.repository.CustomerRepository;
import com.sanchez.SistemaDrugstore.service.CustomerService;


@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;
	
	
	
	public CustomerServiceImpl(CustomerRepository customerRepository) {
	
		this.customerRepository = customerRepository;
	}

	@Override
	public Customer save(Customer customer) {
		// TODO Auto-generated method stub
		return this.customerRepository.save(customer);
	}

	@Override
	public List<Customer> findAll() {
		// TODO Auto-generated method stub
		return this.customerRepository.findAll();
	}

	@Override
	public Optional<Customer> findOne(Integer customerId) {
		// TODO Auto-generated method stub
		return this.customerRepository.findById(customerId);
	}

	@Override
	public void delete(Integer customerId) {
		 Optional<Customer>cust= customerRepository.findById(customerId);
	      	Customer c= cust.get();
	      	c.setDeleted(false);
	}

	@Override
	public void altaCustomer(Integer idCustomer) {
		// TODO Auto-generated method stub
		
	}

}
