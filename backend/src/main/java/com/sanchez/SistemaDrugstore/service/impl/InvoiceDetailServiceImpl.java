package com.sanchez.SistemaDrugstore.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sanchez.SistemaDrugstore.entity.InvoiceDetail;
import com.sanchez.SistemaDrugstore.entity.Product;
import com.sanchez.SistemaDrugstore.repository.InvoiceDetailRepository;
import com.sanchez.SistemaDrugstore.repository.ProductRepository;
import com.sanchez.SistemaDrugstore.service.InvoiceDetailService;

@Service
@Transactional
public class InvoiceDetailServiceImpl implements InvoiceDetailService{

	private final InvoiceDetailRepository invoiceDetailRepository; 
	private final ProductRepository prodRep;
	
	
	
	

	public InvoiceDetailServiceImpl(InvoiceDetailRepository invoiceDetailRepository, ProductRepository prodRep) {
		super();
		this.invoiceDetailRepository = invoiceDetailRepository;
		this.prodRep = prodRep;
	}

	@Override
	public InvoiceDetail save(InvoiceDetail invoiceDetail) {
		// TODO Auto-generated method stub
		return this.invoiceDetailRepository.save(invoiceDetail);
	}

	@Override
	public List<InvoiceDetail> findAll() {
		// TODO Auto-generated method stub
		return this.invoiceDetailRepository.findAll();
	}

	@Override
	public Optional<InvoiceDetail> findOne(Integer invoiceDetailId) {
		// TODO Auto-generated method stub
		return this.invoiceDetailRepository.findById(invoiceDetailId);
	}

	@Override
	public void delete(Integer invoiceDetailId) {
		// TODO Auto-generated method stub
		this.invoiceDetailRepository.deleteById(invoiceDetailId);
		
	}

	@Override
	public boolean validateCantidad(Integer invoiceDetailId) {
		// TODO Auto-generated method stub
		
		Optional<InvoiceDetail> inv= invoiceDetailRepository.findById(invoiceDetailId);
		InvoiceDetail d=inv.get();
		
		if(d.getQuantity()>0) {
			return true;
		}
				
		return false;
	}

	@Override
	public boolean stockVal(Integer idProduct) {
		Integer c=0;
		c= prodRep.controlStock(idProduct);
		if(c>0) {
			return true;
		}
		else
			return false;
	}
	}

	

	


	


