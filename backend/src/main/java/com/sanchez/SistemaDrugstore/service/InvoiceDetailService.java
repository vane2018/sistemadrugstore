package com.sanchez.SistemaDrugstore.service;

import java.util.List;
import java.util.Optional;

import com.sanchez.SistemaDrugstore.entity.Invoice;
import com.sanchez.SistemaDrugstore.entity.InvoiceDetail;
import com.sanchez.SistemaDrugstore.entity.Product;

public interface InvoiceDetailService {
	

   InvoiceDetail save(InvoiceDetail invoiceDetail);

	  List<InvoiceDetail> findAll();

	  Optional<InvoiceDetail> findOne(Integer invoiceDetailId);

	  void delete(Integer invoiceDetailId);
	  
	  boolean validateCantidad(Integer invoiceDetailId);
	  
	  boolean stockVal(Integer idProduct);
	 
}
