package com.sanchez.SistemaDrugstore.service;

import java.util.List;
import java.util.Optional;

import com.sanchez.SistemaDrugstore.entity.Product;

public interface ProductService {

	
	 Product save(Product product);

	  List<Product> findAll();

	  Optional<Product> findOne(Integer productId);

	  void delete(Integer usuarioId);
	  
	  List<Product> productSearch();
	  
	  List<Product> GetSelling();
	  
	 boolean validate(Product product);
	 
	 boolean stockValidar(Integer idProduct);
	  
	  
}
