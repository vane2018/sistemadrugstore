package com.sanchez.SistemaDrugstore.service;

import java.util.List;
import java.util.Optional;

import com.sanchez.SistemaDrugstore.entity.Invoice;
import com.sanchez.SistemaDrugstore.entity.User;


public interface UserService {

	User save(User user);

	  List<User> findAll();

	  Optional<User> findOne(Integer userId);

	  void delete(Integer usuarioId);
	  
	  User searchUser(String name,String password);
	
}
