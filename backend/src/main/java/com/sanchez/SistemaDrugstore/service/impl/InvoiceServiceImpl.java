package com.sanchez.SistemaDrugstore.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sanchez.SistemaDrugstore.dto.InvoiceDTO;
import com.sanchez.SistemaDrugstore.entity.Customer;
import com.sanchez.SistemaDrugstore.entity.Invoice;
import com.sanchez.SistemaDrugstore.entity.InvoiceDetail;
import com.sanchez.SistemaDrugstore.entity.Product;
import com.sanchez.SistemaDrugstore.repository.CustomerRepository;
import com.sanchez.SistemaDrugstore.repository.InvoiceRepository;
import com.sanchez.SistemaDrugstore.repository.ProductRepository;
import com.sanchez.SistemaDrugstore.service.InvoiceDetailService;
import com.sanchez.SistemaDrugstore.service.InvoiceService;
import com.sanchez.SistemaDrugstore.service.ProductService;

@Service
@Transactional
public class InvoiceServiceImpl implements InvoiceService{
private final InvoiceRepository invoiceRepository;
private final ProductRepository productRepository;
private final CustomerRepository customerRepository;
private ProductServiceImpl psi;
private ProductService productService;
private Product p;
private Integer pr=0;
private Integer c= 0;
private Integer contador;
List<Integer> prod= new ArrayList<Integer>();
List<Integer> canti= new ArrayList<Integer>();
List<Integer> valido= new ArrayList<Integer>();


InvoiceDetailService invoiceDet;
private  boolean validacion=false;
boolean [] validacion1 ;
private boolean validacion2=false;
private boolean validacion3=false;
private boolean validacio4=false;
private Integer val5=0;
private Integer val6=0;



	

	public InvoiceServiceImpl(InvoiceRepository invoiceRepository, ProductRepository productRepository,
		CustomerRepository customerRepository) {
	super();
	this.invoiceRepository = invoiceRepository;
	this.productRepository = productRepository;
	this.customerRepository = customerRepository;
}

	@Override
	public List<Invoice> findAll() {
		// TODO Auto-generated method stub
		return this.invoiceRepository.findAll();
	}

	@Override
	public Optional<Invoice> findOne(Integer invoiceId) {
		// TODO Auto-generated method stub
		return this.invoiceRepository.findById(invoiceId);
	}

	@Override
	public void delete(Integer invoiceId) {
		this.invoiceRepository.deleteById(invoiceId);
		
	}

	
	

	@Override
	public double calculateTotal(List<InvoiceDetail> invoiceDetail) {
		double Total=0;
		for (InvoiceDetail id : invoiceDetail) {
				
			Total=(id.subtotal()+Total);
			
		}
		return Total;
	
	}



	@Override
	public Invoice save(Invoice invoice) {
         valido.clear();
		
         invoice.getInvoiceDetail().forEach(x-> {
			 
			   if(x.validarCantidad() == true){
			     validacion=true;
			     
			   
			   }
			   if(x.validateProduct(x.getProduct().getProductId())){
				   
				   Optional<Product>produ= productRepository.findById(x.getProduct().getProductId());
			      	Product p= produ.get();
			       	if((p.getStock()>0 ))
			      	
			    	{
			        	if(validateStockCantidad(p.getStock(), x.getQuantity()) & (p.isDeleted()) & validacion ) {
			        		
							
						  	p.setStock((p.getStock()-(x.getQuantity())));
						
						  	valido.add(1);
						  	
					  	
				}else {valido.add(2);}
			      		
			      		
			      	}else {valido.add(2);}
		
				}
			   
			  
			   }
		 );
         
         invoice.getInvoiceDetail().forEach(x->x.setInvoice(invoice));
		 
	
		 
		 for (Integer g : valido) {
			 if(g==2) {
				 val5=4;
			 }else if (g==1) {
				 val6=6;}
			
		}
		
		 	
		 //si se cumple los metodos de validacion se guarda
	  if((val6==6) & (val5 !=4) ) {
	   return this.invoiceRepository.save(invoice);
	  }else 
	   return null;             
	   
	}
	
	
	

	@Override
	public boolean validateStockCantidad(Integer stock, Integer cantidad) {
		if(stock>=cantidad) {
			return true;
		}else {
		return false;}
	}

	

	   
	


	
	
	
}
	
	
	



























