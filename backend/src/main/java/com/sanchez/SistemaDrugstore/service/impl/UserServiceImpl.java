package com.sanchez.SistemaDrugstore.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sanchez.SistemaDrugstore.entity.User;
import com.sanchez.SistemaDrugstore.repository.UserRepository;
import com.sanchez.SistemaDrugstore.service.UserService;


@Service
@Transactional
public class UserServiceImpl implements UserService {

	private final UserRepository usuarioRepository;
	
	
	
	public UserServiceImpl(UserRepository usuarioRepository) {
		
		this.usuarioRepository = usuarioRepository;
	}

	@Override
	public User save(User user) {
		// TODO Auto-generated method stub
		return this.usuarioRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return this.usuarioRepository.findAll();
	}

	

	@Override
	public void delete(Integer usuarioId) {
		// TODO Auto-generated method stub
		this.usuarioRepository.deleteById(usuarioId);
		
	}

	@Override
	public User searchUser(String name,String password) {
		// TODO Auto-generated method stub
		return this.usuarioRepository.verificarNombreUsuario(name, password);
	}

	@Override
	public Optional<User> findOne(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}



	


	


	



}
