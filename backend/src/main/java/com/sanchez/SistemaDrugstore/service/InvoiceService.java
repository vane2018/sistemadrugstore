package com.sanchez.SistemaDrugstore.service;

import java.util.List;
import java.util.Optional;

import com.sanchez.SistemaDrugstore.dto.InvoiceDTO;
import com.sanchez.SistemaDrugstore.entity.Customer;
import com.sanchez.SistemaDrugstore.entity.Invoice;
import com.sanchez.SistemaDrugstore.entity.InvoiceDetail;
import com.sanchez.SistemaDrugstore.entity.Product;
import com.sanchez.SistemaDrugstore.entity.Invoice;

public interface InvoiceService {
	 
	Invoice save(Invoice invoice);
	

	  List<Invoice> findAll();

	  Optional<Invoice> findOne(Integer invoiceId);

	  void delete(Integer invoiceId);
	  
	  boolean validateStockCantidad(Integer stock,Integer cantidad);
	  

	  
	  double calculateTotal(List<InvoiceDetail> invoiceDetail);


	


	  

	 
}
