package com.sanchez.SistemaDrugstore.service;

import java.util.List;
import java.util.Optional;

import com.sanchez.SistemaDrugstore.entity.Customer;

public interface CustomerService {

	 Customer save(Customer customer);

	  List<Customer> findAll();

	  Optional<Customer> findOne(Integer customerId);

	  void delete(Integer customerId);
	  public void altaCustomer(Integer idCustomer);
}
